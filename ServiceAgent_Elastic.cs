﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebSocketWindowsService
{
    public class ServiceAgent_Elastic
    {
        private OntologyModDBConnector dbReaderPort;
        private OntologyModDBConnector dbReaderActive;
        private OntologyModDBConnector dbReaderPath;
        private OntologyModDBConnector dbReaderModuleOfWebsocketService;
        private OntologyModDBConnector dbReaderWebSocketServices;
        private OntologyModDBConnector dbReaderWebSocketController;
        private OntologyModDBConnector dbReaderSoftwareDevelopmentOfWebsocketController;
        private OntologyModDBConnector dbReaderBaseConfig;
        private OntologyModDBConnector dbReaderComServer;

        private clsLocalConfig localConfig;

        public int Port { get; private set; }
        public string ComServerIp { get; private set; }

        public List<WebSocketService> GetServices()
        {
            var searchWebSocketServices = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = Properties.Settings.Default.ServerId,
                    ID_RelationType = localConfig.OItem_relationtype_uses.GUID,
                    ID_Parent_Other = localConfig.OItem_class_websocketservice.GUID
                }
            };

            var result = dbReaderWebSocketServices.GetDataObjectRel(searchWebSocketServices);

            if (result.GUID == localConfig.Globals.LState_Error.GUID || !dbReaderWebSocketServices.ObjectRels.Any()) return null;

            var searchActive = dbReaderWebSocketServices.ObjectRels.Select(webSocketService => new clsObjectAtt
            {
                ID_Object = webSocketService.ID_Other,
                ID_AttributeType = localConfig.OItem_attributetype_is_active.GUID
            }).ToList();



            var searchPath = dbReaderWebSocketServices.ObjectRels.Select(webSocketService => new clsObjectRel
            {
                ID_Object = webSocketService.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_located_at.GUID,
                ID_Parent_Other = localConfig.OItem_class_path.GUID
            }).ToList();

            var searchModule = dbReaderWebSocketServices.ObjectRels.Select(webSocketService => new clsObjectRel
            {
                ID_Object = webSocketService.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_is.GUID,
                ID_Parent_Other = localConfig.OItem_class_module.GUID
            }).ToList();

            if (searchActive.Any())
            {
                result = dbReaderActive.GetDataObjectAtt(searchActive);
            }
            else
            {
                dbReaderActive.ObjectRels.Clear();
            }

            if (!searchPath.Any() || !searchModule.Any()) return null;

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            result = dbReaderPath.GetDataObjectRel(searchPath);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            result = dbReaderModuleOfWebsocketService.GetDataObjectRel(searchModule);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            var searchWebsocketController = dbReaderWebSocketServices.ObjectRels.Select(sockServ => new clsObjectRel
            {
                ID_Object = sockServ.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                ID_Parent_Other = localConfig.OItem_class_websocketcontroller.GUID
            }).ToList();

            result = dbReaderWebSocketController.GetDataObjectRel(searchWebsocketController);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            var searchSoftwareDevelopment = dbReaderWebSocketController.ObjectRels.Select(websocketController => new clsObjectRel
            {
                ID_Object = websocketController.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_is.GUID,
                ID_Parent_Other = localConfig.OItem_class_software_development.GUID
            }).ToList();

            if (!searchSoftwareDevelopment.Any()) return null;

            result = dbReaderSoftwareDevelopmentOfWebsocketController.GetDataObjectRel(searchSoftwareDevelopment);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            var socketServices = (from webSocketItem in dbReaderWebSocketServices.ObjectRels
                    join activeItem in dbReaderActive.ObjAtts on webSocketItem.ID_Other equals activeItem.ID_Object into activeItems
                    from activeItem in activeItems.DefaultIfEmpty()
                    join pathItem in dbReaderPath.ObjectRels on webSocketItem.ID_Other equals pathItem.ID_Object
                    join devItem in dbReaderModuleOfWebsocketService.ObjectRels on webSocketItem.ID_Other equals devItem.ID_Object
                    orderby webSocketItem.OrderID
                    select new WebSocketService
                    {
                        IsActive = activeItem != null ?  activeItem.Val_Bit.Value : false,
                        Key = webSocketItem.ID_Other,
                        Name = devItem.Name_Other,
                        AssemblyPath = pathItem.Name_Other
                    }).ToList();

            socketServices.ForEach(service =>
            {
                service.WebSocketControllers = (from controller in dbReaderWebSocketController.ObjectRels.Where(contr => contr.ID_Object == service.Key)
                                                join classItem in dbReaderSoftwareDevelopmentOfWebsocketController.ObjectRels on controller.ID_Other equals classItem.ID_Object
                                                select new WebsocketController
                                                {
                                                    Key = controller.ID_Other,
                                                    Name = controller.Name_Other,
                                                    ClassName = classItem.Name_Other
                                                }).ToList();
            });

            return socketServices;
        }
        
        public clsOntologyItem GetData_ComServerIp()
        {
            var searchIp = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = localConfig.OItem_object_baseconfig.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_comserver.GUID,
                    ID_Parent_Other = localConfig.OItem_class_ip_adress.GUID
                }
            };

            var result = dbReaderComServer.GetDataObjectRel(searchIp);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                if (dbReaderComServer.ObjectRels.Any())
                {
                    ComServerIp = dbReaderComServer.ObjectRels.First().Name_Other;
                }
                else
                {
                    result = localConfig.Globals.LState_Relation.Clone();
                }
            }

            return result;
        }

        public clsOntologyItem GetData_Port()
        {
            var searchPort = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = localConfig.OItem_object_baseconfig.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_uses.GUID,
                    ID_Parent_Other = localConfig.OItem_class_port.GUID
                }
            };

            var result = dbReaderPort.GetDataObjectRel(searchPort);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                if (dbReaderPort.ObjectRels.Any())
                {
                    var port = 0;
                    if (int.TryParse(dbReaderPort.ObjectRels.First().Name_Other, out port))
                    {
                        Port = port;
                    }
                    else
                    {
                        result = localConfig.Globals.LState_Error.Clone();
                    }
                }
                else
                {
                    result = localConfig.Globals.LState_Relation.Clone();
                }
            }

            return result;
        }

        public clsOntologyItem SetUserGroupPath()
        {
            var searchBaseResources = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = localConfig.OItem_object_baseconfig.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_datapath.GUID,
                    ID_Parent_Other = localConfig.OItem_class_path.GUID
                }
            };

            var result = dbReaderBaseConfig.GetDataObjectRel(searchBaseResources);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;
            if (!dbReaderBaseConfig.ObjectRels.Any()) return localConfig.Globals.LState_Error.Clone();

            try
            {
                var path = dbReaderBaseConfig.ObjectRels.First().Name_Other;
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                ModuleDataExchanger.UserGroupPath = dbReaderBaseConfig.ObjectRels.First().Name_Other;
            }
            catch (Exception ex)
            {
                return localConfig.Globals.LState_Error.Clone();
            }

            return result;
        }

        public ServiceAgent_Elastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            dbReaderPort = new OntologyModDBConnector(localConfig.Globals);
            dbReaderActive = new OntologyModDBConnector(localConfig.Globals);
            dbReaderPath = new OntologyModDBConnector(localConfig.Globals);
            dbReaderModuleOfWebsocketService = new OntologyModDBConnector(localConfig.Globals);
            dbReaderWebSocketServices = new OntologyModDBConnector(localConfig.Globals);
            dbReaderWebSocketController = new OntologyModDBConnector(localConfig.Globals);
            dbReaderSoftwareDevelopmentOfWebsocketController = new OntologyModDBConnector(localConfig.Globals);
            dbReaderBaseConfig = new OntologyModDBConnector(localConfig.Globals);
            dbReaderComServer = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
