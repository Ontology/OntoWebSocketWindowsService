﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace OntoWebSocketWindowsService
{
    public partial class OntoWebSocketService : ServiceBase
    {

        private clsLocalConfig localConfig;
        private ServiceAgent_Elastic serviceAgentDb;
        private List<WebSocketService> serviceItems;
        private WebSocketServer wssv;
        private EventLog eventLog1;

        public OntoWebSocketService()
        {
            InitializeComponent();

            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("OntoWebSocketServiceSource"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "OntoWebSocketServiceSource", "OntoWebSocketServiceLog");
            }
            eventLog1.Source = "OntoWebSocketServiceSource";
            eventLog1.Log = "OntoWebSocketServiceLog";
        }

        public void Debug()
        {
            OnStart(null);
        }
        protected override void OnStart(string[] args)
        {
            string cwd = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
            Directory.SetCurrentDirectory(cwd ?? ".");

            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            var result = Initialize();

            if (result.GUID == localConfig.Globals.LState_Relation.GUID)
            {
                eventLog1.WriteEntry("The Port is not configured!", EventLogEntryType.Error);
            }
            else if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                eventLog1.WriteEntry("An Error occured!", EventLogEntryType.Error);
            }
            else
            {
                WebSocketBase.ComServerIP = serviceAgentDb.ComServerIp;
                WebSocketBase.ComServerPort = serviceAgentDb.Port;
                ModuleDataExchanger._serviceClosed += ModuleDataExchanger__serviceClosed;
                wssv = new WebSocketServer(serviceAgentDb.Port, true);

                eventLog1.WriteEntry("Created Websocket-Service",EventLogEntryType.Information);
                wssv.SslConfiguration.ServerCertificate = new System.Security.Cryptography.X509Certificates.X509Certificate2("ontosockcert.pfx", "");
                eventLog1.WriteEntry("Added Certificate", EventLogEntryType.Information);
#if DEBUG
                // To change the logging level.
                wssv.Log.Level = LogLevel.Trace;

                // To change the wait time for the response to the WebSocket Ping or Close.
                wssv.WaitTime = TimeSpan.FromSeconds(2);
#endif
                wssv.AddWebSocketService<ComServiceAgent>("/ModuleCommunicator");

                ModuleDataExchanger.WebSocketServices = serviceItems;

                ModuleDataExchanger.WebSocketServices.ForEach(serviceItem =>
                {
                    if (!serviceItem.IsActive) return;

                    serviceItem.WebSocketControllers.ForEach(controller =>
                    {
                        serviceItem.WebSocketPath = "/" + serviceItem.Name + "." + controller.Name;
                        wssv.AddWebSocketService<WebsocketServiceAgent>(serviceItem.WebSocketPath);
                        eventLog1.WriteEntry("Added:" + serviceItem.WebSocketPath, EventLogEntryType.Information);
                    });

                });



                wssv.Start();
                eventLog1.WriteEntry("Started Websocket-Server", EventLogEntryType.Information);
                if (wssv.IsListening)
                {
                    eventLog1.WriteEntry(string.Format("Listening on port {0}, and providing WebSocket services", wssv.Port), EventLogEntryType.Information);
                    foreach (var path in wssv.WebSocketServices.Paths)
                        eventLog1.WriteEntry("Listening: " + path, EventLogEntryType.Information);
                }

               
            }
        }

        private void ModuleDataExchanger__serviceClosed(WebsocketServiceAgent serviceAgent)
        {
            serviceAgent = null;
        }


        private clsOntologyItem Initialize()
        {
            serviceAgentDb = new ServiceAgent_Elastic(localConfig);
            var result = serviceAgentDb.GetData_Port();
            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            serviceItems = serviceAgentDb.GetServices();

            if (serviceItems == null || !serviceItems.Any()) return localConfig.Globals.LState_Error.Clone();

            result = serviceAgentDb.SetUserGroupPath();

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            result = serviceAgentDb.GetData_ComServerIp();

            return result;

        }

        protected override void OnStop()
        {
            
        }
    }
}
